# 7. Be Competitive in the Market

![](images/9.jpg)

## STEP 7

---

Your social-first news strategy must be built with an understanding of the market in which your agency operates. Refer back to your target audience and ask yourselves some questions about the market and the broader media and regulatory environment with your audience in mind.

* How will I attract and build audience?
* What is the role advertisements or sponsorships play currently?
* Who is selling to my audience already?
* Which advertising/sponsorship packages do I want to sell?
* Which other related services do I want to sell (competitor surveillance, content, production)?

There are no one-size-fits-all answers to these questions because each are affected by local regulations, market standards, technical skills and your own editorial focus.

When starting a media business you likely will lack the data -- often called metrics -- needed to identify your audience and its interests and behaviors. Social networking tools have the idea of metrics at their core, it's what their business is built on. Analytics tools examine the path of your users while consuming content, as well as the content’s path on the internet. What is being shared, by whom, and how often?

Tracking and examining this analytics will enable you to create a pool of audience data. When audience data start coming in, you will begin to see the actual profile of your audience. Based on this, you can decide whether it is possible to broaden the commercial target group or, perhaps, start selling packages for more than one target group. Whatever your ultimate strategy for creating revenue, having a clear pool of audience data is the first step.

Be sure to evaluate your competitors and current market, as well as potential audience.

---
