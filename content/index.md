# How to Craft a Social First News Strategy

![](images/1.jpg)

Social platforms today enable news organizations to reach an international audience of unprecedented scale. Significant stories often appear first on Twitter, Facebook and YouTube and then on television outlets such as Al Jazeera, CNN or the BBC. The one-two punch of a viral story online followed by television coverage can quickly give an issue world- wide attention.

Small World News recommends news organizations, especially start-up organizations, adapt to this changing landscape by implementing a social-first news strategy. Deliver the news where the audience already is. Recognize social platforms can help drive the news cycle. Tap the audience to contribute research, fact-checking, and sourcing. Each of these are requirements for the newsrooms of the future.

A social-first strategy will enable a small staff to leverage a large audience and the no-cost content it creates. In addition, this model allows you to quickly and affordably produce content for distribution to larger outlets such as CNN or Al Jazeera. Small organizations that cover large or remote geographic regions can use a social-first approach to quickly source material from previously unreachable areas.

A news story is never truly complete. Though coverage often begins as real-time breaking news, the most important stories are often only understood over time. Comprehensive coverage of important issues over the long haul is a process, a cycle, that news organizations need to understand and control. The real-time, social web makes this more true than ever before.

This guide will help you tell great stories that are meant to be shared by walking you through the process to create a social-first strategy.

---
