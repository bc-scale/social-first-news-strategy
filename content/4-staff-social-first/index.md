# 4. Staff a Social-First Newsroom

## STEP 4

---

Now that you have defined an editorial focus, your audience, and learned to engage them effectively, its time to staff for a social-first strategy. If you are adapting from a traditional newsroom, you may need to make structural changes for a social first plan to work. Remember that as the platform changes, the content, workflow and even job descriptions within your news organization must change too.

![](images/newsroom_staffing.png)

Your staff plan must be designed to produce content in a multimedia, social-first environment. Assemble a team familiar with the tools and audience. This team will need to understand how to shoot video and take photos with an assortment of mobile devices as well as report. It is likely that your team will have specialists – the reporter who is especially good at video or who is interested in children’s issues. That is fine. A good team has a mix of skills that complement each other. In the social-first newsroom the denominator is an understanding of the social environment and a love for reporting in this space.

Your staff needs will most certainly change over time. You should gear up slowly; staff to produce only slightly more material than your current level of demand. Do not fall into the old newsroom trap of too many employees and bloat. At all levels rely on your audience for content.

To ensure you’re staying on top of good news stories you need a clear newsroom structures with defined responsibilities. Specific job titles and the total number of employees will depend on the scope of your editorial plan and the size and interests of your audience. In addition to traditional newsroom job titles, one staff requirement in a social-first newsroom is sure: the Social Editor.

---
## Newsroom Job descriptions
---

## Editor-In-Chief

The editor-in-chief (EiC) is ultimately responsible for overseeing the direction and editorial values of the organization. However, the editor-in-chief must trust editors and staff reporters to make good decisions. Initially the EiC will be in charge of the managing staff and business, but eventually the organization should grow and hire individuals in each of these positions.

## Managing Editor

Initially the managing editor may be the EiC’s right hand or star reporter. As your organization grows, establish a Managing Editor to run the newsroom. While the EiC must manage the business and direct the values of the company, the Managing Editor sets the daily news agenda.

## Social Editor

Someone must take primary responsibility for defining the social strategy and assessing its success. The Social Editor reviews the activity of individual reporters and defines an overall strategy for social media. The Social Editor is responsible for tracking trends, evaluating new tools and innovating on your social-first strategy.

## Business and Marketing Director

Working closely with the EiC, this individual oversees budgetary and marketing decisions. It’s important to keep this individual’s department separate from the newsroom to protect the independence and credibility of the newsroom.

---

Social media engagement does not stop with the social editors. Reporters must be engaged constantly with their social networks too. Not only to stay ahead of stories but to constantly groom sources, enlist contributors and fact-check in the social sphere. Social must be integrated into the fundamental workflow of your news organization at every level.

---
