# 3. Understand What Makes Social-First Stories

![](images/4.jpg)

### STEP 3

---

Engaging with your audience and involving them in the reporting process is key to success in a social-first news strategy. Verification is an important part of the process. Your social media editor and reporters should verify material as it arrives. It helps to have some core contributors among your audience. The track record of new contributors should be cross checked against their social profile and the contributions of others. While you’re doing this, you should be looking for news content – you never know what contributors will dig up.

In 2011, as part of the Alive in Libya project, Small World News started a hashtag [#AskaLibyan](https://twitter.com/search?q=AskaLibyan) to promote engagement between the international community and Libyan citizens. At the time internet connectivity was virtually nonexistent in Libya. Livestreaming would have been prohibitively expensive if not impossible. However Twitter worked well, and provided a low bandwidth opportunity to create a relationship between individuals inside Libya and those abroad.

---

### Characteristics of a Social Strategy

* Engages audience in research/development of primary material.
* Evolves as new information becomes available over time.
* Encourages collaboration, discussion.
* Inspires action.

---

Visuals are essential. Publish raw short video clips or photos with captions as soon as you can. Sharing additional content to deepen the narrative keeps the audience involved and coming back. Your headlines will only go so far without compelling visual content. Invite your audience to share and tag similar material, to help you craft an accurate narrative. Retweet and republish authentic, relevant content, as verified.

As the story cycle progresses build on real-time work as background to build an in-depth, well rounded feature. Remember to engage your most active and reliable audience to seek out new stories. Follow-up on breaking news stories over time. Rinse, and repeat.

---

![](images/MCZ.png)

### Example of a good Facebook post.

---

### Succeeding on Facebook

* Use a short, intriguing title.
* Include an eye catching image with every post.
* Provide a short intro to the piece, rather than posting the entire item.
* Add a text summary even if the content being shared is a video.

---

Whether covering breaking news or planning a feature, remember the social-first model of reporting:

1. Announce upcoming stories early and often, excite your audience to assist in reporting.

2. Publish raw short video clips or photos -- with captions -- as soon as available.

3. Share additional content over the life of the story, to deepen the narrative, and keep the audience involved and coming back.

Your headlines will only go so far without the support of multimedia content. Invite your audience to share and tag similar material, to help you craft an accurate narrative. Retweet and republish authentic, relevant content as it is verified. Use this real-time work to build an in-depth, well-rounded feature. Remember to engage your most active and reliable audience to seek out new stories.

---

![](images/5.jpg)

**In the end, the discipline of verification is what separates journalism from entertainment, propaganda, fiction, or art... Journalism alone is focused first on getting what happened down right.**

**-The Elements of Journalism**

---

As you refine your reporting process, reporters must build relationships with key contributors from the audience. Enabling your audience to create a track record that proves their reliability and credibility will increase the speed and efficiency of reporting over time. Remember that engaging your audience in the process does not mean providing all sources or audience content equal weight. Seek out subject matter experts and contributors with a keen eye for effective, efficient research.

---
